﻿package jjoriping.math{
	import flash.utils.ByteArray;

	public class Algebra {

		private static const ordinal1:Array = ["한","두","세","네","다섯","여섯","일곱","여덟","아홉"];
		private static const ordinal10:Array = ["열","스무","서른","마흔","쉰","예순","일흔","여든","아흔"];
		private static const ordinalNum:Array = ["","일","이","삼","사","오","육","칠","팔","구"];
		private static const ordinalCycle:Array = ["","십","백","천"];
		private static const ordinalOver:Array = ["","만","억","조","경","해"];
		public static const EngMonth:Array = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

		private static var mExp:String;

		public function Algebra() {
			throw new ArgumentError("Error #2012: jjoriping.math.Algebra 클래스를 인스턴스화할 수 없습니다.");
		}

		public static function rate(value:Number):Boolean {
			return rand(1,10000)<=value*100;
		}
		public static function sign(value:Number):Number {
			return Math.abs(value)/value;
		}
		public static function percent(value:Number,per:Number,point:uint=0):Number {
			var tracer:Number = value * per * 0.01;
			return cut(tracer,point);
		}
		public static function toRadian(value:Number):Number{
			return value/180*Math.PI;
		}
		public static function rand(min:Number,max:Number,point:uint=0):Number {
			var tracer:Number;
			if(point==0){
				tracer = Math.floor(min+Math.random()*(max-min+1));
			}else{
				tracer = min+Math.random()*(max-min);
			}
			return cut(tracer,point);
		}
		public static function randSign(val:Number):Number {
			return (rand(0,1)*2-1)*val;
		}
		public static function Arand(array:Object):* {
			return array[rand(0,array.length-1)];
		}
		public static function pick(array:Object):* {
			var I:int = rand(0,array.length-1);
			var O:Object = array[I];
			array.splice(I,1);
			return O;
		}
		public static function cut(val:Number,point:uint=1):Number{
			return Number(val.toFixed(point));
		}
		public static function prettySpace(value:Number):String{
			var S:Number;
			var fix:String;
			if(value<1024){
				S = value;
				fix = "B";
			}else if(value<1048576){
				S = Algebra.cut(value/1024,2);
				fix = "kB";
			}else if(value<1073741824){
				S = Algebra.cut(value/1048576,2);
				fix = "MB";
			}else{
				S = Algebra.cut(value/1073741824,2);
				fix = "GB";
			}
			return S+" "+fix;
		}
		public static function prettyDistance(value:Number):String{
			var S:Number;
			var fix:String;
			if(value<1000){
				S = Algebra.cut(value,2);
				fix = "m";
			}else{
				S = Algebra.cut(value*0.001,2);
				fix = "km";
			}
			return S+fix;
		}
		public static function prettyTime(value:Date,range:String=null):String{
			var R:Array;
			var H:String = String(value.hours);
			var M:String = String(value.minutes);
			var S:String = String(value.seconds);
			switch(range){
				case "M":
					M = String(value.minutes+value.hours*60);
					break;
				default:
			}
			if(H.length == 1){
				H = "0"+H;
			}
			if(M.length == 1){
				M = "0"+M;
			}
			if(S.length == 1){
				S = "0"+S;
			}
			switch(range){
				case "H":
					R = [H,M];
					break;
				case "M":
					R = [M,S];
					break;
				default:
					R = [H,M,S];
			}
			return R.join(":").toString();
		}
		public static function prettyDate(date:Date,lang:String="KR"):String{
			var R:String;
			switch(lang){
				case "KR":
					R = date.fullYear+"년 "+(date.month+1)+"월 "+date.date+"일";
					break;
				case "US":
					R = EngMonth[date.month]+". "+date.date+", "+date.fullYear;
					break;
				default:
			}
			return R;
		}
		public static function prettyKoreanTime(sec:Number,cut:uint=2):String{
			var S:Array = new Array();
			if(sec>=3600){
				S.push(int(sec/3600)+"시간");
			}
			if(sec>=60){
				S.push(int(sec/60)%60+"분");
			}
			S.push(Algebra.cut(sec%60,cut)+"초");
			return S.join(" ").toString();
		}
		public static function prettyNumber(value:Number):String {
			var res:String = String(Algebra.cut(value-int(value),2)).slice(1);
			var M:String = String(int(value));
			var Mt:uint = 0;
			var ML:uint = M.length;
			for (var I:uint=0; I<ML-2; I++) {
				if ((ML-I-1)%3==0) {
					M = M.substr(0,I+1+Mt)+","+M.slice(I+1+Mt);
					Mt++;
				}
			}
			return M+res;
		}
		public static function prettyObject(obj:*, lev:uint=0, space:String="\t", forceCasting:Boolean=false):String{
			try{
				obj = JSON.parse(String(obj));
			}catch(e){
			}finally{
				obj = isNaN(Number(obj))?obj:Number(obj);
			}
			if(obj is String || obj is Number){
				return repeat(space, lev)+obj+"\n";
			}
			function repeat(str:String, num:uint):String{
				var R:String = "";
				for(var i:uint=0; i<num; i++){
					R += str;
				}
				return R;
			}
			function toObj(obj:Object):Object{
				var R:Object = new Object();
				var b:ByteArray = new ByteArray();
				b.writeObject(obj);
				b.position = 0;
				R = b.readObject();
				return R;
			}
			var R:String = "";
			for(var I:String in obj){
				var isnObj:Boolean = obj[I] is String || obj[I] is Number || (obj[I] == null);
				var isArr:Boolean = obj[I] is Array;
				var isJSON:Boolean = obj[I] is String;
				var json:Object;
				if(isJSON){
					try{
						json = JSON.parse(obj[I]);
						isnObj = false;
					}catch(e){
						isJSON = false;
					}finally{
						if(!isNaN(Number(obj[I]))){
							isnObj = true;
							isJSON = false;
						}
					}
				}
				R += repeat(space, lev)+I+": "+(isArr?("Array (길이 "+obj[I].length+")"):(isJSON?"[object JSON]":obj[I]))+"\n";
				if(!isnObj || isArr){
					R += prettyObject(isJSON?json:(forceCasting?toObj(obj[I]):obj[I]), lev+1, space);
				}
			}
			return R;
		}
		public static function koreanNumber(value:Number,ordinal:Boolean=false):String {
			if (ordinal) {
				var gV:String = String(Math.floor(Math.abs(value)));
				if (gV=="0") {
					return "#ERROR";
				}
				if (gV=="1") {
					return "첫";
				}
				var gVL:uint = gV.length;
				if (gV!="1" && gVL==1) {
					return ordinal1[Number(gV)-1];
				}
				var myC:Array = new Array();
				for (var J:uint=0; J<gVL; J++) {
					var gVs:uint = gVL - J;
					var gVc:uint = Number(gV.charAt(J));
					if (gVs>2) {
						myC.push((gVc==1?(gVs==5?(Number(gV.slice(Math.max(gVL-8,0),gVL-4))==1?"":"일"):((gVs-1)%4==0?"일":"")):(ordinalNum[gVc]))+(gVc==0?"":ordinalCycle[(gVs-1)%4])+((gVs-1)%4==0?(ordinalOver[int((gVs-1)/4)]+" "):""));
					}
					if (gVs==2) {
						if (gVc!=0) {
							myC.push(ordinal10[gVc-1]);
						}
					}
					if (gVs==1) {
						if (gVc!=0) {
							myC.push(ordinal1[gVc-1]);
						} else {
							if (gV.charAt(J-1)=="2") {
								myC[myC.length - 1] = "스물";
							}
						}
					}
				}
				if (myC[myC.length - 1].charAt(myC[myC.length - 1].length - 1) == " ") {
					myC[myC.length - 1] = myC[myC.length - 1].slice(0,myC[myC.length - 1].length - 1);
				}
				return myC.join("").toString();
			}
			var myS:Array = new Array();
			if (value>=100000000000000000000 && value%1000000000000000000000000 != 0) {
				myS.push(int(value/100000000000000000000)%10000+"해");
			}
			if (value>=10000000000000000 && value%100000000000000000000 != 0) {
				myS.push(int(value/10000000000000000)%10000+"경");
			}
			if (value>=1000000000000 && value%10000000000000000 != 0) {
				myS.push(int(value/1000000000000)%10000+"조");
			}
			if (value>=100000000 && value%1000000000000 != 0) {
				myS.push(int(value/100000000)%10000+"억");
			}
			if (value>=10000 && int(value/10000)%10000 != 0) {
				myS.push(int(value/10000)%10000+"만");
			}
			if (value%10000 != 0) {
				myS.push(value%10000);
			}
			if(myS.length==0){
				myS.push(0);
			}
			return myS.join(" ").toString();
		}
		public static function GCD(a:Number,b:Number):Number{
			var t:int;
			while (b != 0){
				t = b;
				b = a % b;
				a = t;
			}
			return a;
		}
		public static function LCM(a:Number,b:Number):Number{
			return a * b / GCD(a,b);
		}
		public static function requestParseMath(data:String):Number{
			mExp = data;
			return PM0();
		}
		private static function shiftMExp():void{
			mExp = mExp.slice(1);
		}
		private static function PM0():Number{ // 더하기, 빼기
			var d:Array = [0,0];
			d[0] = PM1();
			if(mExp.length > 0){
				var C:String = mExp.charAt();
				if(C == "+"){
					shiftMExp();
					d[1] = PM0();
					d[0] += d[1];
				}else if(C == "-"){
					shiftMExp();
					d[1] = PM0();
					d[0] -= d[1];
				}
			}
			return d[0];
		}
		private static function PM1():Number{ // 곱하기, 나누기
			var d:Array = [0,0];
			d[0] = PM2();
			if(mExp.length > 0){
				var C:String = mExp.charAt();
				if(C == "*"){
					shiftMExp();
					d[1] = PM1();
					d[0] *= d[1];
				}else if(C == "/"){
					shiftMExp();
					d[1] = PM1();
					d[0] /= d[1];
				}
			}
			return d[0];
		}
		private static function PM2():Number{ // 거듭제곱
			var d:Array = [0,0];
			d[0] = PM_F();
			if(mExp.length > 0){
				var C:String = mExp.charAt();
				if(C == "^"){
					shiftMExp();
					d[1] = PM2();
					d[0] = Math.pow(d[0],d[1]);
				}
			}
			return d[0];
		}
		private static function PM_F():Number{ // 괄호
			var d:Number = 0;
			if(mExp.length > 0){
				var C:String = mExp.charAt();
				if(isNumber(C)){
					d = PM_N();
				}else if(C == "("){
					shiftMExp();
					d = PM0();
				}else if(C == ")"){
					shiftMExp();
				}
			}
			return d;
		}
		private static function PM_N():Number{
			var R:String = "";
			var C:String = mExp.charAt();
			while(isNumber(C)){
				R += C;
				shiftMExp();
				if(mExp.length == 0){
					break;
				}
				C = mExp.charAt();
			}
			return Number(R);
		}
		private static function isNumber(s:String):Boolean{
			return (!isNaN(Number(s)) || s == ".");
		}
	}

}