﻿package jjoriping.tools{
	import flash.media.SoundChannel;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	import flash.net.SharedObject;
	import flash.geom.Point;
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.utils.getTimer;
	import jjoriping.math.Algebra;
	import flash.net.FileReference;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.System;
	import flash.net.FileFilter;
	import flash.display.DisplayObjectContainer;
	import flash.ui.Keyboard;
	import flash.text.TextFieldAutoSize;

	public class Box {

		private static const HOTKEY_LABEL:Array = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0","`","-","=","\\","[","]",";","'",",",".","/","Esc","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","백스페이스","엔터","스페이스 바","Tab","Insert","Delete","Home","End","Page Up","Page Down","←","↓","→","↑","숫자패드 1","숫자패드 2","숫자패드 3","숫자패드 4","숫자패드 5","숫자패드 6","숫자패드 7","숫자패드 8","숫자패드 9","숫자패드 0","숫자패드 .","숫자패드 엔터","숫자패드 +","숫자패드 -","숫자패드 *","숫자패드 /","Caps Lock"];
		private static const HOTKEY_DATA:Array = [Keyboard.A,Keyboard.B,Keyboard.C,Keyboard.D,Keyboard.E,Keyboard.F,Keyboard.G,Keyboard.H,Keyboard.I,Keyboard.J,Keyboard.K,Keyboard.L,Keyboard.M,Keyboard.N,Keyboard.O,Keyboard.P,Keyboard.Q,Keyboard.R,Keyboard.S,Keyboard.T,Keyboard.U,Keyboard.V,Keyboard.W,Keyboard.X,Keyboard.Y,Keyboard.Z,Keyboard.NUMBER_1,Keyboard.NUMBER_2,Keyboard.NUMBER_3,Keyboard.NUMBER_4,Keyboard.NUMBER_5,Keyboard.NUMBER_6,Keyboard.NUMBER_7,Keyboard.NUMBER_8,Keyboard.NUMBER_9,Keyboard.NUMBER_0,Keyboard.BACKQUOTE,Keyboard.MINUS,Keyboard.EQUAL,Keyboard.BACKSLASH,Keyboard.LEFTBRACKET,Keyboard.RIGHTBRACKET,Keyboard.SEMICOLON,Keyboard.QUOTE,Keyboard.COMMA,Keyboard.PERIOD,Keyboard.SLASH,Keyboard.ESCAPE,Keyboard.F1,Keyboard.F2,Keyboard.F3,Keyboard.F4,Keyboard.F5,Keyboard.F6,Keyboard.F7,Keyboard.F8,Keyboard.F9,Keyboard.F10,Keyboard.F11,Keyboard.F12,Keyboard.BACKSPACE,Keyboard.ENTER,Keyboard.SPACE,Keyboard.TAB,Keyboard.INSERT,Keyboard.DELETE,Keyboard.HOME,Keyboard.END,Keyboard.PAGE_UP,Keyboard.PAGE_DOWN,Keyboard.LEFT,Keyboard.DOWN,Keyboard.RIGHT,Keyboard.UP,Keyboard.NUMPAD_1,Keyboard.NUMPAD_2,Keyboard.NUMPAD_3,Keyboard.NUMPAD_4,Keyboard.NUMPAD_5,Keyboard.NUMPAD_6,Keyboard.NUMPAD_7,Keyboard.NUMPAD_8,Keyboard.NUMPAD_9,Keyboard.NUMPAD_0,Keyboard.NUMPAD_DECIMAL,Keyboard.NUMPAD_ENTER,Keyboard.NUMPAD_ADD,Keyboard.NUMPAD_SUBTRACT,Keyboard.NUMPAD_MULTIPLY,Keyboard.NUMPAD_DIVIDE,Keyboard.CAPS_LOCK];

		public static var SC:SoundChannel = new SoundChannel();
		public static var SC_BGM:SoundChannel = new SoundChannel();
		private static var STF:SoundTransform = new SoundTransform();
		private static var byte_SC_BGM:int;
		private static var timeoutV:uint = 0;
		public static var explCycle:uint = 30;
		public static var isDisplaying:Boolean = false;
		public static const BAR:String = "\n<p align='center'>──────────────────────────────</p>\n";
		public static var myStage:Stage;
		public static var currentBGM:Sound;
		public static var currentLyrics:XML;
		
		private static var fadeTimer:int = -1;

		private static var _mute:Boolean = false;
		public static function get mute():Boolean{
			return _mute;
		}
		public static function set mute(val:Boolean):void{
			_mute = val;
			var st:SoundTransform;
			st = SC_BGM.soundTransform;
			st.volume = val?0:1;
			SC_BGM.soundTransform = st;
			st = SC.soundTransform;
			st.volume = 0;
			SC.soundTransform = st;
		}
		private static var expls:Object = new Object();
		public static var explSprite:Sprite = new Sprite();
		explSprite.graphics.beginFill(0,0.7);
		explSprite.graphics.drawRect(0,0,100,100);
		explSprite.visible = false;
		public static var explText:TextField = new TextField();
		private static var explTF:TextFormat = new TextFormat("Malgun Gothic",12,0xFFFFFF);
		explText.defaultTextFormat = explTF;
		explText.selectable = false;
		explText.autoSize = TextFieldAutoSize.LEFT;

		public static var currentRect:Sprite = new Sprite();
		private static var recentTimer:Number;

		public function Box() {
			throw new ArgumentError("Error #2012: jjoriping.tools.Box 클래스를 인스턴스화할 수 없습니다.");
		}
		public static function playSound(sound:Class,vol:Number=1):void {
			if(mute){
				return;
			}
			SC = new sound().play();
			STF.volume = vol;
   			SC.soundTransform = STF;
		 }
		private static function _playSound(sound:Sound,vol:Number=1):void {
			if(mute){
				return;
			}
			SC = sound.play();
			STF.volume = vol;
   			SC.soundTransform = STF;
		 }
		public static function setMouseEnabled(val:Boolean=false):void{
			explSprite.mouseEnabled = val;
			explText.mouseEnabled = val;
			currentRect.mouseEnabled = val;
		}
		public static function stopSound():void {
			SC.stop();
		}
		public static function playBGM(sound:Class,vol:Number=1,fade:Boolean=false):void {
			if(fadeTimer != -1){
				stopBGM();
				clearTimeout(fadeTimer);
				fadeTimer = -1;
			}
			var myBGM:Sound = new sound();
			_playBGM(myBGM,vol,fade);
		}
		private static function _playBGM(sound:Sound,vol:Number=1,fade:Boolean=false):void{
			if(mute){
				return;
			}
			if (sound.bytesTotal != byte_SC_BGM) {
				SC_BGM.stop();
				currentBGM = sound;
				SC_BGM = sound.play(0,int.MAX_VALUE);
				byte_SC_BGM = sound.bytesTotal;
				STF.volume = fade?0:vol;
				SC_BGM.soundTransform = STF;
				if(fade){
					setTimeout(playingBGM,50,SC_BGM.soundTransform.volume);
				}
			}
		}
		private static function playingBGM(val:Number):void {
			if (val>=1) {
				return;
			}
			setBGMVolume(val+0.05);
			setTimeout(playingBGM,50,SC_BGM.soundTransform.volume);
		}
		public static function stopBGM(fade:Boolean=false):void {
			byte_SC_BGM = 0;
			if (fade) {
				fadeTimer = setTimeout(stoppingBGM,50,SC_BGM.soundTransform.volume);
			} else {
				SC_BGM.stop();
			}
		}
		private static function stoppingBGM(val:Number):void {
			if (val<=0) {
				fadeTimer = -1;
				return;
			}
			setBGMVolume(val-0.05);
			fadeTimer = setTimeout(stoppingBGM,50,SC_BGM.soundTransform.volume);
		}
		public static function parseGomLyrics(lyrics:String):XML{
			var lyric:XML;
			System.useCodePage = false;
			try{
				lyric = XML(lyrics);
			}catch(e:*){
				var pusher:uint=0;
				var ok:Boolean = true;
				while(ok){
					try{
						pusher++;
						lyric = XML(lyrics.slice(0,lyrics.length-pusher));
						ok = false;
					}catch(e:*){
						if(pusher>=1000){
							ok = false;
						}
					}
				}
			}
			return lyric;
		}
		public static function getGomLyricsPosition():int{
			var V:int;
			for(var I:String in currentLyrics.lyrics.lyric.body.sync){
				if(currentLyrics.lyrics.lyric.body.sync[I].@start > SC_BGM.position){
					V = int(I)-1;
					break;
				}
			}
			return V;
		}
		public static function setBGMVolume(vol:Number):void {
			var myST:SoundTransform = SC_BGM.soundTransform;
			myST.volume = vol;
			SC_BGM.soundTransform = myST;
		}
		public static function makeButton(obj:DisplayObject,listener:Function,expl:String=null,type:String=MouseEvent.MOUSE_DOWN):void {
			if(!obj.hasEventListener(type)){
				obj.addEventListener(type,listener);
			}
			if (expl!=null) {
				makeExpl(obj,expl);
			}
		}
		public static function makeScrollButton(obj:DisplayObject,subj:Sprite,expl:String=null):void{
			obj.addEventListener(MouseEvent.MOUSE_DOWN,doScroll);
			function doScroll(e:MouseEvent):void{
				obj.stage.addEventListener(MouseEvent.MOUSE_UP,haltScroll);
				subj.startDrag();
			}
			function haltScroll(e:MouseEvent):void{
				obj.stage.removeEventListener(MouseEvent.MOUSE_UP,haltScroll);
				subj.stopDrag();
			}
		}
		public static function objectLength(obj:Object):uint{
			var R:uint = 0;
			for(var I:String in obj){
				R++;
			}
			return R;
		}
		public static function enableExpl(container:DisplayObjectContainer,gap:Point=null):void{
			myStage = container.stage;
			container.addChild(explSprite);
			container.addChild(explText);
			container.addChild(currentRect);
			container.addEventListener(Event.ENTER_FRAME,EF);
			function EF(e:Event):void {
				var X:Number = myStage.mouseX + gap.x;
				if(X+explSprite.width>myStage.stageWidth){
					X = myStage.mouseX - explSprite.width - gap.x;
				}
				var Y:Number = myStage.mouseY - explSprite.height - gap.y;
				if(Y<0){
					Y = myStage.mouseY + gap.y;
				}
				explToCursor(X,Y);
			}
		}
		public static function makeExpl(obj:DisplayObject,text:String):void {
			if(text==""){
				delete expls[obj.name];
				delete expls["!!"+obj.name+"!!"];
				return;
			}
			expls[obj.name] = "<p align='left'>"+text+"</p>";
			var explRect:Sprite = new Sprite();
			explRect.graphics.beginFill(0,0);
			explRect.graphics.drawRect(0,1,100,100);
			explRect.width = obj.width;
			explRect.height = obj.height;
			explRect.mouseEnabled = false;
			var objP:* = obj;
			var objPP:* = obj;
			var myObjP:Point = new Point(obj.x,obj.y);
			try{
			while(objPP.name!=null){
				if(objP.parent!=null){
					myObjP.x += objP.parent.x;
					myObjP.y += objP.parent.y;
					objP = objP.parent;
					objPP = objP;
				}else{
					if(!objP.hasOwnProperty("_parent")){
						break;
					}
					objPP = objP._parent;
					objP = objPP;
				}
			}
			}catch(e){trace("[jjoriping.tools.Box 경고] "+objP.name+"의 부모가 존재하지 않습니다.\n***상세 오류:\n"+e);};
			explRect.x = myObjP.x;
			explRect.y = myObjP.y;
			expls["!!" + obj.name + "!!"] = [explRect,obj];
			if (! obj.hasEventListener(MouseEvent.MOUSE_OVER)) {
				obj.addEventListener(MouseEvent.MOUSE_OVER,displayExpl);
			}
		}
		public static function disposeExpl():void{
			var DC:uint=0;
			for(var I in expls){
				if(I.charAt(0)=="!"){
					if(I,expls[I][1].stage == null){
						var N:String = I.split("!!")[1];
						delete expls[I];
						delete expls[N];
						DC++;
					}
				}
			}
			if(DC>0){
				trace("[jjoriping.tools.Box 클래스 알림] "+DC+"개의 null 객체 설명이 제거되었습니다.");
			}
		}
		private static function displayExpl(e:MouseEvent):void {
			if (timeoutV != 0) {
				clearTimeout(timeoutV);
				timeoutV = 0;
			}
			if (!expls.hasOwnProperty("!!"+e.currentTarget.name+"!!")) {
				trace("[jjoriping.tools.Box 경고] 등록되지 않은 설명에 접근 : "+e.currentTarget.name);
				return;
			}
			makeExpl(DisplayObject(e.currentTarget),expls[e.currentTarget.name]);
			currentRect = expls["!!" + e.currentTarget.name + "!!"][0];
			explText.htmlText = expls[e.currentTarget.name];
			explSprite.width = explText.width;
			explSprite.height = explText.height + 5;
			isDisplaying = true;
			setTimeout(displayingExpl,explCycle,explSprite.alpha);
			e.currentTarget.addEventListener(MouseEvent.MOUSE_OUT,hideExpl);
		}
		private static function displayingExpl(alpha:Number):void {
			if (! isDisplaying) {
				return;
			}
			explSprite.visible = true;
			explText.visible = true;
			currentRect.visible = true;
			explSprite.alpha = alpha;
			explText.alpha = explSprite.alpha;
			if (explSprite.alpha < 1) {
				setTimeout(displayingExpl,explCycle,alpha+0.1);
			}
		}
		public static function hideExpl(e:MouseEvent=null):void {
			if (timeoutV != 0 && isDisplaying) {
				clearTimeout(timeoutV);
				timeoutV = 0;
			}
			isDisplaying = false;
			timeoutV = setTimeout(hidingExpl,explCycle,explSprite.alpha);
			if(e != null){
				e.currentTarget.removeEventListener(MouseEvent.MOUSE_OUT,hideExpl);
			}
		}
		private static function hidingExpl(alpha:Number):void {
			if (isDisplaying) {
				return;
			}
			explSprite.alpha = alpha;
			explText.alpha = explSprite.alpha;
			if (explSprite.alpha > 0) {
				setTimeout(hidingExpl,explCycle,alpha-0.1);
			} else {
				explText.visible = false;
				explSprite.visible = false;
				currentRect.visible = false;
			}
		}
		public static function explToCursor(X:Number,Y:Number):void {
			explSprite.x = X;
			explSprite.y = Y;
			explText.x = X;
			explText.y = Y;
		}
		public static function step(init:Boolean=false):void{
			if(isNaN(recentTimer) || init){
				recentTimer = getTimer();
				trace("STEP: 초기화");
				return;
			}
			trace("STEP: "+(getTimer()-recentTimer));
			recentTimer = getTimer();
		}
		public static function getKeyName(keys:Array):String{
			var R:Array = new Array();
			if(keys.length>1){
				for(var i:int=1;i<keys.length;i++){
					R.push({
						C:"Ctrl",
						S:"Shift",
						A:"Alt"
					}[keys[i]]);
				}
			}
			i = HOTKEY_DATA.indexOf(keys[0]);
			if(i != -1){
				R.push(HOTKEY_LABEL[i]);
			}
			return R.join("+").toString();
		}
		public static function gotoRandomFrame(obj:MovieClip,andPlay:Boolean=false):void {
			var goF:uint = Algebra.rand(1,obj.totalFrames);
			if (andPlay) {
				obj.gotoAndPlay(goF);
				return;
			}
			obj.gotoAndStop(goF);
		}
		public static function createCode(len:uint=8):String{
			var s:String = "";
			for(var I:uint=0;I<len;I++){
				var v:uint = Algebra.rand(48,90);
				while(v>=57 && v<=65){
					v = Algebra.rand(48,90);
				}
				s+=String.fromCharCode(v);
			}
			return s;
		}
		public static function save(name:String,data:Object):void {
			step(true);
			var so:SharedObject = SharedObject.getLocal(name);
			for (var I:* in data) {
				so.data[I] = data[I];
			}
			so.data.STAMP = "STAMPED BY jjoriping.tools.Box";
			so.flush();
			step();
		}
		public static function load(name:String):Object {
			var so:SharedObject = SharedObject.getLocal(name);
			if (! so.data.hasOwnProperty("STAMP")) {
				trace("[jjoriping.tools.Box 경고] STAMP 속성이 대상 경로에 존재하지 않습니다. 저장 정보를 불러올 수 없습니다.");
				return null;
			}
			var ro:Object = new Object;
			for (var I:* in so.data) {
				ro[I] = so.data[I];
			}
			return ro;
		}
		public static function copyObject(source:Object):Object{
			var NO:Object = new Object();
			for(var I in source){
				var C:Class = source[I]["constructor"];
				if(C==Object){
					NO[I] = copyObject(source[I]);
				}else if(C==Array){
					NO[I] = copyArray(source[I]);
				}else{
					NO[I] = source[I];
				}
			}
			return NO;
		}
		public static function copyArray(source:Array):Array{
			var NA:Array = new Array();
			for(var I in source){
				var C:Class = source[I]["constructor"];
				if(C==Object){
					NA[I] = copyObject(source[I]);
				}else if(C==Array){
					NA[I] = copyArray(source[I]);
				}else{
					NA[I] = source[I];
				}
			}
			return NA;
		}

	}

}