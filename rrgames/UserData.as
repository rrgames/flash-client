﻿package rrgames {
	import rrgames.Agent;
	import jjoriping.math.Algebra;
	import flash.external.ExternalInterface;
	
	public class UserData {

		public static var my:UserData;
		internal static var token:String;
		
		public var id:String;	// 서버에서의 ID
		public var topRank:int;
		public var profile:Profile;
		public var $ping:Number;	// 돈
		public var $jjo:Number;	// 돈
		public var friendList:Vector.<UserData>;
		
		public function UserData() {

			id = "test"+Algebra.rand(100000,999999);
			profile = new Profile();
			profile.nickname = "실험"+Algebra.rand(1000,9999);
			$ping = Algebra.rand(100000,999999);
			friendList = new Vector.<UserData>();
			if(Game.DEBUG){
				topRank = 1004;
			}
			
		}
		public function getFriendById(key:String):UserData{
			var R:UserData;
			for(var I:String in friendList){
				if(friendList[I].id == key){
					R = friendList[I];
					break;
				}
			}
			return R;
		}

		public static function getMyData(listener:Function=null):void{
			Agent.query(Agent.FACEBOOK_LOGIN, null, function(data:Object):void{
				if(data.type != Agent.GET_SESSION_TOKEN){
					ExternalInterface.call("alert", "올바른 호출이 아닙니다!");
					return;
				}
				Agent.query(Agent.GET_SESSION_TOKEN, {token: data.data}, function(data:Object):void{
					data = data.session;

					my.id = data.user_id;
					token = data.session_token;

					if(listener != null){
						listener();
					}
				});
			});
		}
		public static function getMyGameData(listener:Function=null):void{
			Agent.query(Agent.GET_USER_DATA, {id: my.id}, function(data:Object):void{
				data = data.user;
				my.profile.nickname = data.name;
				var tr:Array = data.top_rank.split("-");
				if(tr.length == 2){
					my.topRank = int(tr[0])*1000 + int(tr[1]);
				}else{
					my.topRank = 1000;
				}
				my.$jjo = data.money_jjo;
				my.$ping = data.money_ping;
				my.profile.imageURL = data.profile_image//.replace("0.0.50.50/p50x50", "0.0.120.120/p120x120");
				my.profile.uid = data.facebook_uid;
				if(listener != null){
					listener();
				}
			});
		}
		public static function getMyFriendList(listener:Function=null):void{
			Agent.query(Agent.GET_FRIEND_LIST, null, function(data:Object):void{
				my.friendList = new Vector.<UserData>();
				data.friends.forEach(function(val:Object, index:int, arr:Array):void{
					var ud:UserData = new UserData();
					ud.id = val.id;
					ud.profile.nickname = val.name;
					ud.profile.imageURL = val.profile_image//.replace("0.0.50.50/p50x50", "0.0.120.120/p120x120");
					my.friendList.push(ud);
				});
				if(listener != null){
					listener();
				}
			});
		}
		public static function getMyFriendData(listener:Function=null):void{
			/* 스테이지 진도에 대한 정보가 없음
				진도 정보를 받는 대로 이 부분 활성화
			*/
			if(listener != null){
				listener();
			}
		}

	}
	
}
