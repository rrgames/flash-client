﻿package rrgames {
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	
	public class Expl {

		private static var expls:Object = new Object();
		private static var currentRect:Sprite;
		private static var timeoutV:uint;
		public static var explObj:Object;
		
		public static function makeExpl(obj:DisplayObject,text:String):void {
			if(text == ""){
				delete expls[obj.name];
				delete expls["!!"+obj.name+"!!"];
				return;
			}
			expls[obj.name] = "<p align='left'>"+text+"</p>";
			var explRect:Sprite = new Sprite();
			explRect.graphics.beginFill(0,0);
			explRect.graphics.drawRect(0,1,100,100);
			explRect.width = obj.width;
			explRect.height = obj.height;
			explRect.mouseEnabled = false;
			var objP:* = obj;
			var objPP:* = obj;
			var myObjP:Point = new Point(obj.x,obj.y);
			try{
				while(objPP.name != null){
					if(objP.parent != null){
						myObjP.x += objP.parent.x;
						myObjP.y += objP.parent.y;
						objP = objP.parent;
						objPP = objP;
					}else{
						if(!objP.hasOwnProperty("_parent")){
							break;
						}
						objPP = objP._parent;
						objP = objPP;
					}
				}
			}catch(e){trace("[rrgames.Expl 클래스 경고] "+objP.name+"의 부모가 존재하지 않습니다.\n***상세 오류:\n"+e);};
			explRect.x = myObjP.x;
			explRect.y = myObjP.y;
			expls["!!" + obj.name + "!!"] = [explRect,obj];
			if (! obj.hasEventListener(MouseEvent.MOUSE_OVER)) {
				obj.addEventListener(MouseEvent.MOUSE_OVER,displayExpl);
			}
		}
		public static function disposeExpl():void{
			var DC:uint=0;
			for(var I in expls){
				if(I.charAt(0) == "!"){
					if(I,expls[I][1].stage == null){
						var N:String = I.split("!!")[1];
						delete expls[I];
						delete expls[N];
						DC++;
					}
				}
			}

			if(DC>0){
				trace("[rrgames.Expl 클래스 알림] "+DC+"개의 null 객체 설명이 제거되었습니다.");
			}
		}
		private static function displayExpl(e:MouseEvent):void {
			if (timeoutV != 0) {
				clearTimeout(timeoutV);
				timeoutV = 0;
			}
			if (!expls.hasOwnProperty("!!"+e.currentTarget.name+"!!")) {
				trace("[rrgames.Expl 경고] 등록되지 않은 설명에 접근 : "+e.currentTarget.name);
				return;
			}
			makeExpl(DisplayObject(e.currentTarget),expls[e.currentTarget.name]);
			currentRect = expls["!!" + e.currentTarget.name + "!!"][0];
			explObj.T.htmlText = expls[e.currentTarget.name];
			explObj.P.width = explObj.T.width+10;
			explObj.P.height = explObj.T.height+10;
			explObj.visible = true;
			explObj.alpha = 1;
			e.currentTarget.addEventListener(MouseEvent.MOUSE_OUT,hideExpl);
		}
		public static function hideExpl(e:MouseEvent=null):void {
			if (timeoutV != 0) {
				clearTimeout(timeoutV);
				timeoutV = 0;
			}
			timeoutV = setTimeout(hidingExpl,30,explObj.alpha);
			if(e != null){
				e.currentTarget.removeEventListener(MouseEvent.MOUSE_OUT,hideExpl);
			}
		}
		private static function hidingExpl(alpha:Number):void {
			explObj.alpha = alpha;
			if (explObj.alpha > 0.1) {
				timeoutV = setTimeout(hidingExpl,30,alpha-0.1);
			} else {
				explObj.visible = false;
				currentRect.visible = false;
			}
		}

	}
	
}
