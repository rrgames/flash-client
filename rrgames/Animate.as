﻿package rrgames {
	import flash.display.Bitmap;
	import flash.display.IBitmapDrawable;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.events.Event;
	
	public class Animate extends Sprite {

		public static const STAGE_SPLIT_VERT:String = "stageSplitVert";
		public static const STAGE_COMPRESS_HORZ:String = "stageCompressHorz";
		
		public var display:IBitmapDrawable;
		public var bitmap:Vector.<Bitmap>;
		public var bitmapData:Vector.<BitmapData>;
		public var type:String;
		
		private var patch:Object;
		
		public function Animate(display:IBitmapDrawable, type:String=STAGE_SPLIT_VERT){
			bitmap = new Vector.<Bitmap>();
			bitmapData = new Vector.<BitmapData>();
			this.display = display;
			this.type = type;
			patch = new Object();
		}
		public function stageAndPlay(mother:Object):void{
			mother.addChild(this);
			play();
		}
		private function complete():void{
			while(numChildren){
				removeChild(getChildAt(0));
			}
			graphics.clear();
			removeEventListener(Event.ENTER_FRAME, efHandler);
			parent.removeChild(parent.getChildByName(name));
			dispatchEvent(new Event(Event.COMPLETE));
		}
		public function play():void{
			switch(type){
				case STAGE_SPLIT_VERT:	// 스테이지 시작 직전 애니메이션
					bitmapData.push(new BitmapData(400, 570, true, 0));
					bitmapData[0].draw(display, new Matrix(1,0,0,1,0,-70));
					bitmap.push(new Bitmap(bitmapData[0]));
					bitmap[0].y = 70;
					addChild(bitmap[0]);

					bitmapData.push(new BitmapData(320, 570, true, 0));
					bitmapData[1].draw(display, new Matrix(1,0,0,1,-400,-70));
					bitmap.push(new Bitmap(bitmapData[1]));
					bitmap[1].x = 400;
					bitmap[1].y = 70;
					addChild(bitmap[1]);
				
					graphics.beginFill(0x000000);
					graphics.drawRect(0, 70, 720, 570);
					patch.dX = 0;
					addEventListener(Event.ENTER_FRAME, efHandler);
					break;
				case STAGE_COMPRESS_HORZ:	// 스테이지 결과 확인 직후 애니메이션
					bitmapData.push(new BitmapData(720, 580, true, 0));
					bitmapData[0].draw(display, new Matrix(1,0,0,1,0,-60));
					bitmap.push(new Bitmap(bitmapData[0]));
					bitmap[0].y = 60;
					addChild(bitmap[0]);

					graphics.beginFill(0x000000);
					graphics.drawRect(0, 60, 720, 580);
					addEventListener(Event.ENTER_FRAME, efHandler);
					break;
				default:
			}
		}
		private function efHandler(e:Event):void{
			switch(type){
				case STAGE_SPLIT_VERT:
					patch.dX += 0.7;
					bitmap[0].x -= patch.dX;
					bitmap[1].x += patch.dX;
					if(bitmap[0].x < -400){
						complete();
					}
					break;
				case STAGE_COMPRESS_HORZ:
					bitmap[0].scaleY *= 0.9;
					bitmap[0].alpha = bitmap[0].scaleY;
					bitmap[0].y = 350-bitmap[0].height*0.5;
					if(bitmap[0].scaleY < 0.06){
						bitmap[0].scaleY = 0;
						complete();
					}
					break;
				default:
			}
		}

	}
	
}
