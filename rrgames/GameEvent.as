﻿package rrgames {
	import flash.events.Event;
	
	public class GameEvent extends Event {

		public static const SCORE_CHANGED:String = "scoreChanged";
		
		public var data:Object;
		
		public function GameEvent(type:String, data:Object=null) {
			super(type);
			this.data = data;
		}

	}
	
}
