﻿package rrgames.stage {
	
	public class StageRule {

		public var gravity:Boolean;
		public var infinite:Boolean;
		public var colorFlag:Array;
		public var forbidItem:Array;
		
		public function StageRule(gravity:String=null, infinite:String=null, colorFlag:String=null, forbidItem:String=null) {
			if(gravity != null){
				this.gravity = gravity == "true";
			}
			if(infinite != null){
				this.infinite = infinite == "true";
			}
			if(colorFlag != null){
				this.colorFlag = colorFlag.split("");
			}
			if(forbidItem != null){
				this.forbidItem = forbidItem.split("");
			}else{
				this.forbidItem = new Array();
			}
		}
		public function parseFlag():Array{
			var R:Array = new Array();
			colorFlag.forEach(function(data:String, index:int, my:Array):void{
				if(data == "1"){
					R.push(StageMap.COLOR_FLAG[index]);
				}
			});
			return R;
		}
		public function toString():String{
			var R:String = "";
			
			R = [gravity, infinite, colorFlag, forbidItem].toString();
			
			return R;
		}

	}
	
}
