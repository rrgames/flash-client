﻿package rrgames.stage {
	import jjoriping.math.Algebra;
	import rrgames.Game;
	import jjoriping.tools.Box;
	
	public class StageMission {
		
		public static const MISSION_TYPE:Array = [
			["AC"],
			["CB", "DB", "TL", "ML", "PM", "SC"]
		]
		public static const MISSION_ACT:Object = {
			/* MISSION_ACT 항목 양식
				{title: 제목 (이 속성이 없으면 짧게, 있으면 길게 표시)
					desc: 설명 (인자에 따라 설명 제공)
				}
			*/
			AC: {
				desc: function(arg:Array=null):String{
					return "화면 비우기";
				},
				aft: function(game:Game, mission:StageMission):String{
					var left:int = 0;
					for(var x:int=0; x<game.data.map.width; x++){
						for(var y:int=0; y<game.data.map.height; y++){
							if(game.data.map.map[y][x] != -1){
								left++;
							}
						}
					}
					return "앞으로 <font color='#FFFFFF' size='16px'>"+left+"</font>개";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return game.isClear();
				}
			},
			CB: {title: "블럭 제거<font size='14px'> (색)</font>",
				desc: function(arg:Array):String{
					return StageMap.COLOR_DESC[arg[0]]+" 블럭 "+arg[1]+"개 제거";
				},
				aft: function(game:Game, mission:StageMission):String{
					return "앞으로 <font color='#FFFFFF' size='16px'>"+Algebra.prettyNumber(mission.data[1]-mission.current[1])+"</font>개";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return mission.current[1] >= mission.data[1];
				}
			},
			DB: {title: "블럭 제거<font size='14px'> (위치)</font>",
				desc: function(arg:Array):String{
					return "(X: "+arg[0]+", Y: "+arg[1]+")에 있는 블럭 제거";
				},
				aft: function(game:Game, mission:StageMission):String{
					return "";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return mission.current[2];
				}
			},
			ML: {
				desc: function(arg:Array):String{
					return "<font size='14px'>제한 턴 </font><font color='#FFFF00'>"+arg[0]+"</font>회";
				},
				bonus: function(game:Game, mission:StageMission):Number{
					var r:int = Math.max(0, mission.data[0]/game.move-1);
					return mission.data[0]*r*416;
				},
				aft: function(game:Game, mission:StageMission):String{
					var bt:String = (game.bonusMove>0)?("<font size='11px' color='#00FFFF'>+"+game.bonusMove):"";
					return "<font size='14px'>제한 턴 </font><font size='16px' color='#FFFF00'>"+game.move+"</font>/<font size='12px'>"+mission.data[0]+bt+"회</font>";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return game.move < (Number(mission.data[0])+game.bonusMove);
				}
			},
			PM: {title: "블럭 제거<font size='14px'> (한꺼번에)</font>",
				desc: function(arg:Array):String{
					return "<font color='#FFFFFF'>"+StageMap.COLOR_DESC[arg[0]]+"</font> 블럭 한꺼번에 <font color='#FFFFFF'>"+arg[1]+"</font>개 제거";
				},
				aft: function(game:Game, mission:StageMission):String{
					return "현재 최고 <font color='#FFFFFF' size='16px'>"+Algebra.prettyNumber(mission.current[1])+"</font>개";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return mission.current[1] >= mission.data[1];
				}
			},
			SC: {
				desc: function(arg:Array):String{
					return Algebra.prettyNumber(arg[0])+"<font size='14px'>점 달성</font>";
				},
				aft: function(game:Game, mission:StageMission):String{
					return "앞으로 <font color='#FFFFFF' size='16px'>"+Algebra.prettyNumber(mission.data[0]-game._score)+"</font>점";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return game._score >= mission.data[0];
				}
			},
			TL: {
				desc: function(arg:Array):String{
					return "<font size='14px'>제한 시간 </font><font color='#FFFF00'>"+Algebra.prettyKoreanTime(arg[0])+"</font>";
				},
				bonus: function(game:Game, mission:StageMission):Number{
					return (mission.data[0]-game.time)*100;
				},
				aft: function(game:Game, mission:StageMission):String{
					return "<font size='14px'>제한 시간 </font><font color='#FFFF00'>"+Algebra.prettyKoreanTime(Math.max(0, mission.data[0]-game.time))+"</font>";
				},
				comp: function(game:Game, mission:StageMission):Boolean{
					return game.time <= mission.data[0];
				}
			}
		}
		
		public var type:String;
		public var data:Array;
		public var current:Array;
		
		public function StageMission(text:String=null) {
			type = text.substr(0, 2);
			data = text.slice(2).split(",");
		}
		public function isCompleted(game:Game):Boolean{
			return MISSION_ACT[type].comp(game, this);
		}
		public function getBonus(game:Game):Number{
			var R:Number;
			
			if(!MISSION_ACT[type].hasOwnProperty("bonus")){
				R = 0;
			}else{
				R = MISSION_ACT[type].bonus(game, this);
			}
			
			return R;
		}
		public function getDescription(now:Boolean=false, game:Game=null):String{
			var R:String;
			if(now){
				R = MISSION_ACT[type].aft(game, this);
			}else{
				R = MISSION_ACT[type].desc(data);
			}
			return R;
		}
		public function toString():String{
			var R:String = "";
			
			R = type+data.toString();
			
			return R;
		}

	}
	
}
