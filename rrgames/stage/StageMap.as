﻿package rrgames.stage {
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import rrgames.Game;
	
	public class StageMap {

		public static const COLOR_FLAG:Array = [
			"R", "Y", "G", "B", "P", "N", "W", "E"
		]
		public static const COLOR_DESC:Object = {
			R: "빨강",
			Y: "노랑",
			G: "초록",
			B: "파랑",
			P: "보라",
			N: "방해",
			W: "벽",
			E: "공백"
		}
		public static function parse(text:String):StageMap{
			var R:StageMap = new StageMap();
			var O:Object = JSON.parse(text);
			var p:Object = new Object();
			
			p.rule = O.rule.split(",");
			R.rule = new StageRule(p.rule[0], p.rule[1], p.rule[2]);
			R.mission = O.mission.split("!");
			R.mission.forEach(function(item:String, index:int, my:Array):void{
				my[index] = new StageMission(item);
			});
			R.width = O.width;
			R.height = O.height;
			R.map = O.map.split("!");
			R.map.forEach(function(item:String, index:int, my:Array):void{
				my[index] = item.split(",");
			});
			
			return R;
		}
		
		public var grade:Array;	// Number의 연속
		public var rule:StageRule;
		public var mission:Array;	// StageMission 인스턴스를 원소로 삼음
		public var width:uint;
		public var height:uint;
		public var map:Array;	// 2중 배열, map[Y][X]로 접근
		public var description:String;
		
		public function StageMap(text:String=null) {
			if(text == null) return;
			var xml:XMLList = new XMLList(text);
			
			parse(xml);
		}
		public function parse(xml:XMLList):void{
			var p:Object = new Object();

			grade = [Number(xml.grade.L1), Number(xml.grade.L2), Number(xml.grade.L3)];
			rule = new StageRule(xml.rule.gravity, xml.rule.infinite, xml.rule.colorflag, xml.rule.forbiditem);
			mission = new Array();
			p.c = 0;
			while(true){
				p.o = xml.missions.mission[p.c];
				if(p.o == undefined) break;
				mission.push(new StageMission(p.o.text()));
				p.c++;
			}
			width = xml.width;
			height = xml.height;
			map = new Array();
			var str:String = xml.map.text();
			var ci:int = 0;
			var ri:int = 0;
			var pa:String = "";
			var c:String = "";
			while(true){
				c = str.charAt(ci);
				if(!c) break;
				pa += c;
				ci++;
				if(c == "N"){
					do{
						c = str.charAt(ci);
						if(c == "") break;
						if(!isNaN(Number(c))){
							pa += c;
						}else{
							break;
						}
						ci++;
					}while(true);
				}else if(c == "I"){
					pa += str.charAt(ci);
					ci++;
				}
				ri++;
				if(ri%width == 0){
					map.push(pa);
					pa = "";
				}
			}
			map.forEach(function(item:String, index:int, my:Array):void{
				map[index] = item.match(/([A-HJ-Z][0-9]{0,2})|(I[A-Z0-9])/gi);
			});
			description = xml.description;
		}
		public function getMissionText():String{
			var R:Array = new Array();
			
			mission.forEach(function(item:StageMission, index:int, my:Array):void{
				R.push(item.getDescription());
			});
			
			return R.join("\n").toString();
		}
		public function renderMap(mother:DisplayObjectContainer, block:Class, clickListener:Function=null, map:Array=null, inGame:Boolean=false):Array{
			if(!map) map = this.map;
			var R:Array = new Array();
			var x:uint;
			var y:uint;
			var fy:uint = 0;
			var r:Array;
			var M:MovieClip;
			Game.mother = mother;
			Game.clickListener = clickListener;
			var ct:String;
			
			var rh:int = map.length;
			for(y=Math.max(0, rh-9); y<rh; y++){
				r = new Array();
				for(x=0; x<width; x++){
					M = new block;
					M.x = x*M.width;
					M.y = fy*M.height;
					M.pos = new Point(x,fy);
					ct = map[y][x].charAt();
					if(ct == "I"){
						ct = map[y][x];
					}
					M.gotoAndStop("B"+ct);
					if(ct == "N"){
						if(M.HT){
							M.HT.text = map[y][x].slice(1);
						}
					}
					mother.addChild(M);
					r.push(M);
					if(clickListener != null){
						M.addEventListener(MouseEvent.CLICK, clickListener);
					}
				}
				fy++;
				R.push(r);
			}
			if(inGame){
				mission.forEach(function(item:StageMission, index:int, my:Array):void{
					if(item.type == "DB"){
						M = new block;
						M.gotoAndStop("DBFlag");
						M.x = item.data[0]*50;
						M.y = item.data[1]*50;
						mother.addChild(M);
					}
				});
			}
			return R;
		}
		public function renderMission(mother:DisplayObjectContainer, panel:Class, clickListener:Function=null):Array{
			var R:Array = new Array();
			var y:uint = 0;
			var M:MovieClip;
			
			mission.forEach(function(data:StageMission, index:int, my:Array):void{				
				M = new panel;
				M.y = y;
				M.data = data;
				M.index = index;
				var md:Object = StageMission.MISSION_ACT[data.type];
				if(md.hasOwnProperty("title")){
					M.gotoAndStop(2);
					M.TitleT.htmlText = md.title;
					M.TextT.htmlText = md.desc(data.data);
					M.NowT.text = "";
					M.update(true);
					y += 85;
				}else{
					M.gotoAndStop(1);
					M.TitleT.htmlText = md.desc(data.data);
					y += 35;
				}
				R.push(M);
				mother.addChild(M);
			});
			
			return R;
		}
		public function getGrade(score:Number):int{
			if(!grade) return 0;
			var R:int = 0;
			while(grade[R]){
				if(grade[R] > score){
					break;
				}
				R++;
			}
			return R;
		}
		public function getObtainPing(score:Number):int{
			var gd:int = getGrade(score);
			if(score >= 2*grade[2]-grade[1]){
				gd++;
			}
			return gd*100;
		}
		public function toString():String{
			var R:String = "";
			
			var missionText:Array = new Array();
			mission.forEach(function(item:StageMission, index:int, my:Array):void{
				missionText.push(item.toString());
			});
			R = JSON.stringify({
				rule: rule.toString(),
				mission: missionText.join("!").toString(),
				width: width,
				height: height,
				map: map.join("!").toString()
			});
			
			return R;
		}

	}
	
}
