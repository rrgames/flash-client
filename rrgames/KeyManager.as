﻿package rrgames {
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.display.DisplayObject;
	
	public class KeyManager {

		public static var stage:Stage;
		public static var root:DisplayObject;
		
		public static function init(root:DisplayObject):void{
			KeyManager.root = root;
			stage = root.stage;
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
		}
		private static function keyDownHandler(e:KeyboardEvent):void{
			var tag:String = root["currentFrameLabel"];
			
			switch(e.keyCode){
				case Keyboard.R:
					if(e.ctrlKey){
						if(root["DEBUG"] && tag == "게임"){
							Game.start(Game.now.data);
							root["initGame"]();
						}
					}
					break;
				case Keyboard.LEFT:
					if(tag == "퍼즐"){
						root["requestMoveTheme"](-1);
					}
					break;
				case Keyboard.RIGHT:
					if(tag == "퍼즐"){
						root["requestMoveTheme"](1);
					}
					break;
				case Keyboard.UP:
					if(tag == "퍼즐"){
						root["upScrollHandler"]();
					}
					break;
				case Keyboard.DOWN:
					if(tag == "퍼즐"){
						root["downScrollHandler"]();
					}
					break;
				default:
			}
		}

	}
	
}
