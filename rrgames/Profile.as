﻿package rrgames {
	import flash.utils.ByteArray;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.external.ExternalInterface;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	public class Profile {
		
		public var nickname:String;
		private var _imageURL:String;
		public function get imageURL():String{
			return _imageURL;
		}
		public function set imageURL(val:String):void{
			_imageURL = val;
			loadImage();
		}
		public var image:Loader;
		public var uid:String;

		public function Profile() {
			image = new Loader();
		}
		private function loadImage():void{
			try{
				image.load(new URLRequest(_imageURL));
			}catch(e){
				ExternalInterface.call("console.warn", "[내부 오류]", e.toString());
			}
		}
		public function toBitmap(width:Number, height:Number):Bitmap{

			var bd:BitmapData;
			if(Game.DEBUG){
				bd = new BitmapData(width, height, false, 0xCCFFCC);
			}else{
				bd = new BitmapData(image.width, image.height);
				if(image.content != null){
					bd.draw(image);
				}
			}
			var b:Bitmap = new Bitmap(bd);
			b.width = width;
			b.height = height;
			return b;

		}

	}
	
}
